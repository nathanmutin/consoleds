extern crate ds as libds;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate structopt;

use std::io;
use tui::Terminal;
use tui::backend::{TermionBackend, Backend};
use tui::widgets::*;
use tui::layout::*;
use tui::style::*;
use termion::raw::IntoRawMode;
use termion::cursor::Goto;
use termion::event::Key;
use unicode_width::UnicodeWidthStr;

use std::sync::{Arc, Mutex};
use std::thread;
use std::io::Write;
use std::time::Instant;
use std::num::NonZeroU8;
use std::fs::{self, File};

use libds::*;

use chrono::Local;

mod event;
mod input;
mod util;

mod ui;

use self::ui::*;
use self::input::*;
use self::event::*;

type Result<T> = std::result::Result<T, failure::Error>;

use failure::bail;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "consoleds", about = "A TUI Driver Station for controlling FRC Robots", rename_all = "kebab-case")]
struct Config {
    #[structopt(short, long)]
    team_number: u32,
    #[structopt(long)]
    target: Option<String>,
    #[structopt(short = "c", long)]
    alliance_colour: String,
    #[structopt(short = "p", long)]
    alliance_position: u8,
}

fn main() -> Result<()> {
    let conf = Config::from_args();
    let alliance = {
        let pos = if conf.alliance_position < 1 || conf.alliance_position > 3 {
            println!("Invalid position \"{}\". Must be between 1 and 3", conf.alliance_position);
            return Ok(());
        } else {
            conf.alliance_position
        };

        match conf.alliance_colour.to_lowercase().as_str() {
            "red" => Alliance::new_red(pos),
            "blue" => Alliance::new_blue(pos),
            _ => {
                println!("Invalid alliance colour. Got \"{}\". Expected \"red\" or \"blue\"", conf.alliance_colour);
                return Ok(());
            }
        }
    };
    let mut ds = if let Some(ref target) = &conf.target {
        DriverStation::new(&target, conf.team_number, alliance)
    } else {
        DriverStation::new_team(conf.team_number, alliance)
    };

    //std::env::current_exe().unwrap().parent().unwrap().parent().unwrap().parent().unwrap().display() = exe_directory - "target/debug/exe_name" = project_directory
    let mut log_file = File::create(format!("{}/logs/stdout-{}.log", std::env::current_exe().unwrap().parent().unwrap().parent().unwrap().parent().unwrap().display(), Local::now()))?;

    // Go into raw mode and initialize tui-rs backend w/ Termion
    let stdout = io::stdout().into_raw_mode()?;
    let mut backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let mut events = Events::new();
    terminal.clear()?;
    terminal.hide_cursor()?;

    // Thread that updates the state of buttons
    thread::spawn(|| {
        gil_ticker();
    });

    let mut list_items = Arc::new(Mutex::new(vec![]));

    let mut items_cons = list_items.clone();
    ds.set_tcp_consumer(move |packet| {
        let TcpPacket::Stdout(msg) = packet;
        // Clear the list otherwise we start running off the screen (on my machine)
        // Maybe find a way to dynamically determine this number
        let mut items = items_cons.lock().unwrap();
        if items.len() > 35 {
            items.clear();
        }
        log_file.write_all(format!("[{:.4}] {}\n", msg.timestamp, msg.message).as_bytes());
        items.push(msg.message);
    });

    // Set up joystick input callback
    ds.set_joystick_supplier(joystick_callback);

    // History/state for voltage graph and game data
    let mut past_voltages = vec![];

    loop {
        terminal.draw(|mut f| {
            // First set of chunks lays out header and body
            let chunks = Layout::default()
                .constraints([
                    Constraint::Percentage(20),
                    Constraint::Percentage(80)
                ].as_ref())
                .direction(Direction::Vertical)
                .split(f.size());

            // Sorta gross, but creates a hierarchy for what should be displayed, ending with Disabled.
            let trace = ds.trace();
            let t = if !trace.is_connected() {
                Text::styled("No Connection", Style::default().fg(Color::Red).modifier(Modifier::Italic))
            } else if !trace.is_code_started() {
                Text::styled("No Robot Code", Style::default().fg(Color::Red))
            } else if ds.estopped() {
                Text::styled("Emergency Stopped", Style::default().fg(Color::Red).modifier(Modifier::Blink))
            } else if ds.enabled() {
                Text::styled("Enabled", Style::default().fg(Color::Yellow))
            } else {
                Text::raw("Disabled")
            };

            // Break up the header for voltage, trace, and mode
            let top_chunks = Layout::default()
                .constraints([
                    Constraint::Percentage(30),
                    Constraint::Percentage(20),
                    Constraint::Percentage(20),
                    Constraint::Percentage(30)
                ].as_ref())
                .direction(Direction::Horizontal)
                .split(chunks[0]);

            draw_status(&mut f, t, top_chunks[0]);

            let mode = match ds.mode() {
                Mode::Autonomous => "Autonomous",
                Mode::Teleoperated => "Teleoperated",
                Mode::Test => "Test"
            };

            draw_mode(&mut f, mode, top_chunks[1]);

            draw_team_info(&mut f, conf.team_number, top_chunks[2]);

            // Gets the current voltage and adds it to the graph
            let voltage = ds.battery_voltage();
            past_voltages.push(voltage.round() as u64);

            draw_voltages(&mut f, &mut past_voltages, voltage, top_chunks[3]);

            // Breaks apart middle into stdout and instructions for usage
            let middle_chunks = Layout::default()
                .constraints([
                    Constraint::Percentage(50),
                    Constraint::Percentage(50)
                ].as_ref())
                .direction(Direction::Horizontal)
                .split(chunks[1]);

            draw_instructions(&mut f, middle_chunks[0]);

            // Get a lock on the stdout iterator and add all the items in it to the stdout filed
            let mut items: Vec<Text> = list_items.lock().unwrap().clone()
                .into_iter().map(|s| Text::raw(format!("{}\n", s))).collect();
            Paragraph::new(
                items.iter()
            )
                .block(Block::default().borders(Borders::ALL).title("stdout"))
                .wrap(true)
                .render(&mut f, middle_chunks[1]);
        })?;

        match events.next()? {
            Event::Input(input) => {
                // Logic for writing GSM is different, L and R are added, most other keys ignored
                // Command characters for the DS
                match input {
                    Key::Char('q') => break,
                    Key::Char('e') => ds.enable(),
                    Key::Char('\n') => ds.disable(),
                    Key::Char(' ') => ds.estop(),
                    Key::Ctrl('c') => ds.restart_code(),
                    Key::Ctrl('r') => ds.restart_roborio(),
                    Key::Char('a') => ds.set_mode(Mode::Autonomous),
                    Key::Char('t') => ds.set_mode(Mode::Teleoperated),
                    Key::Char('T') => ds.set_mode(Mode::Test),
                    _ => {}
                }
            }
            // Just here to redraw every once in a while
            Event::Tick => {}
        }
    }
    // Cleanup to return the terminal to normal
    terminal.show_cursor()?;
    terminal.clear()?;
    Ok(())
}
