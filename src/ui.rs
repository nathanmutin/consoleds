use tui::{Terminal, Frame};
use tui::backend::{TermionBackend, Backend};
use tui::widgets::*;
use tui::layout::*;
use tui::style::*;

pub fn draw_status<B>(f: &mut Frame<B>, txt: Text, area: Rect)
    where B: Backend
{
    Paragraph::new([txt].iter())
        .block(Block::default()
            .borders(Borders::TOP | Borders::LEFT | Borders::BOTTOM)
            .title("Console DS")
            .title_style(Style::default().fg(Color::Yellow)))
        .render(f, area);
}

pub fn draw_mode<B>(f: &mut Frame<B>, mode: &str, area: Rect)
    where B: Backend
{
    Paragraph::new([Text::raw(mode)].iter())
        .block(Block::default()
            .borders(Borders::TOP | Borders::BOTTOM)
            .title("Mode")
            .title_style(Style::default().fg(Color::Magenta))
        )
        .render(f, area);
}

pub fn draw_team_info<B>(f: &mut Frame<B>, team: u32, area: Rect)
    where B: Backend
{
    Paragraph::new([Text::raw(&format!("Team {}", team))].iter())
        .block(Block::default()
            .borders(Borders::TOP | Borders::BOTTOM)
            .title("DS Info")
            .title_style(Style::default().fg(Color::LightMagenta)))
        .render(f, area);
}

pub fn draw_voltages<B>(f: &mut Frame<B>, past_voltages: &mut Vec<u64>, voltage: f32, area: Rect)
    where B: Backend
{
    let colour = if voltage >= 8.5 && voltage < 11.5 {
        Color::Yellow
    } else if voltage < 8.5 {
        Color::Red
    } else {
        Color::Green
    };

    let voltage_chunks = Layout::default()
        .constraints([
            Constraint::Percentage(60),
            Constraint::Percentage(40),
        ].as_ref())
        .split(area);

    Paragraph::new([Text::raw(format!("{:.2} V", voltage))].iter())
        .block(Block::default()
            .borders(Borders::TOP | Borders::RIGHT))
        .style(Style::default().fg(colour))
        .render(f, voltage_chunks[0]);

    if past_voltages.len() > 55 {
        past_voltages.clear();
        past_voltages.push(voltage.round() as u64);
    }

    let mut block = Block::default()
        .borders(Borders::BOTTOM | Borders::RIGHT);

    if *past_voltages.iter().max().unwrap_or(&0) > 0 {
        Sparkline::default()
            .data(&past_voltages[..])
            .block(block)
            .style(Style::default()
                .fg(colour))
            .render(f, voltage_chunks[1]);
    }else {
        block.render(f, voltage_chunks[1]);
    }
}

pub fn draw_instructions<B>(f: &mut Frame<B>, area: Rect)
    where B: Backend
{
    let mut t = vec![
        Text::styled("a", Style::default().fg(Color::Cyan)),
        Text::raw("/"),
        Text::styled("t", Style::default().fg(Color::Cyan)),
        Text::raw("/"),
        Text::styled("T", Style::default().fg(Color::Cyan)),
        Text::raw(": Change the mode between autonomous, teleoperated, and Test\n"),
        Text::styled("e", Style::default().fg(Color::Cyan)),
        Text::raw(": enable the robot\n"),
        Text::styled("Enter", Style::default().fg(Color::Cyan)),
        Text::raw(": disable the robot\n"),
        Text::styled("SPC", Style::default().fg(Color::Cyan)),
        Text::raw(": E-Stop the robot\n"),
        Text::styled("C-c", Style::default().fg(Color::Cyan)),
        Text::raw(": Restart robot code\n"),
        Text::styled("C-r", Style::default().fg(Color::Cyan)),
        Text::raw(": Reboot roboRIO\n"),
        Text::styled("q", Style::default().fg(Color::Cyan)),
        Text::raw(": Quit the application\n")
    ];

    Paragraph::new(t.iter())
        .block(Block::default()
            .title("Usage Instructions")
            .title_style(Style::default().fg(Color::Blue).modifier(Modifier::Bold).modifier(Modifier::Underline))
            .borders(Borders::ALL))
        .wrap(true)
        .render(f, area);
}
